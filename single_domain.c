#ifdef SINGLEDOMAIN

#include "domain.h"

/*
 Single process version of multi process code. Nothing fun here. 
 */


int init_domain(const int size, const int rank, const int nx, const int ny){
	x_dim=nx+2;
	y_dim=ny+2;
	if (size!=1){
		printf("Single domain decomposition is only compatible with single process. \n");
		return 1;
	}
	return 0;
}

int populate_domain(const float *masteredges, const int rank, const int nx, const int ny){
	edges=arralloc(sizeof(float), 2, x_dim, y_dim);
	new_image=arralloc(sizeof(float), 2, x_dim, y_dim);
	old_image=arralloc(sizeof(float), 2, x_dim, y_dim);
	for (int i=1;i<x_dim-1;i++){
		for (int j=1;j<y_dim-1;j++){
			edges[i][j]=masteredges[(i-1)*(ny)+j-1];
		}
	}
	for (int i=0;i<x_dim;i++){
		for (int j=0;j<y_dim;j++){
			old_image[i][j]=new_image[i][j]=255.0;
		}
	}
	return 0;
}

int gather_domain(float *masterimage, const int rank, const int nx, const int ny){
	for (int i=0;i<nx;i++){
		for (int j=0;j<ny;j++){
			masterimage[i*ny+j]=old_image[i+1][j+1];
		}
	}
	return 0;
}

int free_domain(){
	free(edges);
	free(new_image);
	free(old_image);
	return 0;
}

int send_halo(){
	return 0;
}

int receive_halo(){
	return 0;
}

float termination_delta(){
	float delta=0.0;
	for (int i=1;i<x_dim-1;i++){
		for (int j=0;j<y_dim-1;j++){
			float next=fabs(old_image[i][j]-new_image[i][j]);
			if (delta<next){
				delta=next;
			}
		}
	}
	return delta;
}


#endif /* SINGLEDOMAIN */
