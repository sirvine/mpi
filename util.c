
#include "util.h"



void allocate_timer(Timer *t, int capacity){
	t->capacity=capacity;
	t->samples=calloc(sizeof(double), capacity+1);
}

void start_timer(Timer *t){
	t->samples[0]=MPI_Wtime();
	t->n_samples=0;
}

void take_measurement(Timer *t){
	t->samples[t->n_samples+1]=MPI_Wtime();
	t->n_samples++;
}

double mean_period(Timer *t){
	double total=0.0;
	for (int i=1;i<=t->n_samples;i++){
		double dt=(t->samples[i])-(t->samples[i-1]);
		total+=dt;
	}
	return total/t->n_samples;
}

double var_period(Timer *t){
	double var=0.0;
	for (int i=1;i<=t->n_samples;i++){
		double dt=(t->samples[i])-(t->samples[i-1]);
		var+=dt*dt;
	}
	var/=t->n_samples;
	double mean=mean_period(t);
	var-=mean*mean;
	return var; 
}

void free_timer(Timer *t){
	free(t->samples);
}

void swap_ptr(void **a, void **b){
	void *tmp;
	tmp=*a;
	*a=*b;
	*b=tmp;
}

