/* 
 * File:   pgmio.h
 * Author: h
 *
 * Created on 02 November 2013, 19:49
 */

#ifndef PGMIO_H
#define	PGMIO_H

#ifdef	__cplusplus
extern "C" {
#endif

int pgmsize(const char *filename, const int *nx, const int *ny);
int pgmread(const char *filename, const void *vx, const int nx, const int ny);
int pgmwrite(const char *filename, const void *vx, const int nx, const int ny);

#ifdef	__cplusplus
}
#endif

#endif	/* PGMIO_H */


