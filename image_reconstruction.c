
#include "image_reconstruction.h"
#include "util.h"

int read_edges(const int size, const int rank, const char *infilename){
	int dims[2];//nx, ny
	float *masteredges;
	int read_success;
	if (rank==0){
		if (pgmsize(infilename, &nx, &ny)!=0){
			nx=ny=-1;
		}
		dims[0]=nx;
		dims[1]=ny;
		printf("Read dimensions of: %d, %d. \n", nx, ny);
	}
	MPI_Bcast(dims, 2, MPI_INT, 0, MPI_COMM_WORLD); 
	nx=dims[0];
	ny=dims[1];
	if (nx<1||ny<1){
		return -2;
	}
	
	/*Domain dependant code*/
	if (init_domain(size, rank, nx, ny)!=0){
		return -3;
	}
	
	if (rank==0){
		masteredges=calloc(nx*ny, sizeof(float));
		read_success=pgmread(infilename, masteredges, nx, ny)!=0;
	}
	MPI_Bcast(&read_success, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if (read_success!=0){
		if (rank==0)
			free(masteredges);
		return -4;
	}
	
	/*Domain dependant code*/
	if (rank==0)
		printf("Attempting to populate domains. \n");
	if (populate_domain(masteredges, rank, nx, ny)!=0){
		free(masteredges);
		return -5;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank==0){
		printf("Successfully populated domains. \n");
		free(masteredges);
	}
	return 0;
}


/*
 Image reconstruction loop.
 */
int tight_loop(){
	
	init_halo_swap();
		
	for (int i=2;i<x_dim-2;i++){//compute domain
		for (int j=2;j<y_dim-2;j++){
			new_image[i][j]=0.25*(old_image[i-1][j]+old_image[i][j-1]
				+old_image[i][j+1]+old_image[i+1][j]-edges[i][j]);
		}
	}
			
	complete_halo_swap();
			
	for (int j=1;j<y_dim-1;j++){//compute halo edges with fixed i
		for (int i=1;i<x_dim;i+=x_dim-3){
			new_image[i][j]=0.25*(old_image[i-1][j]+old_image[i][j-1]
				+old_image[i][j+1]+old_image[i+1][j]-edges[i][j]);
		}
	}
	for (int i=2;i<x_dim-2;i++){//compute halo edges with fixed j
		for (int j=1;j<y_dim;j+=(y_dim-3)){
			new_image[i][j]=0.25*(old_image[i-1][j]+old_image[i][j-1]
				+old_image[i][j+1]+old_image[i+1][j]-edges[i][j]);
		}
	}
			
	swap_ptr((void**)&old_image, (void**)&new_image);
}

int terminate(){
	if (termination_delta()<TERMINATION_DELTA){
		return 0;
	}
	return 1;
}

int compute_image(const int size, const int rank){
	if (rank==0)
		printf("Beginning reconstruction loop. \n");
#ifndef BENCHMARK	
	int nrounds=0;
	do{
		for (int i=0;i<10;i++){
			tight_loop();
			nrounds++;
		}
	}while(terminate()!=0);
	if (rank==0)
		printf("Delta reached in %d rounds. ", nrounds);
#else
	int n_samples=5;
	Timer timer;
	allocate_timer(&timer, n_samples);
	start_timer(&timer);
	for (int i=0;i<n_samples;i++){
		for(int j=0;j<BENCHMARK_ROUNDS;j++){
			tight_loop();
		}
		take_measurement(&timer);
	}
	if (rank==0)
		printf("Mean time: %g, Variance: %g \n", mean_period(&timer), var_period(&timer));
	free_timer(&timer);
#endif
	
	if (rank==0)
		printf("Finished reconstruction loop. \n");
	return 0;
}
	
int write_image(const int size, const int rank, const char *outfilename){
	float *masterimage;
	int write_success;
	if (rank==0){
		masterimage=calloc(nx*ny, sizeof(float));
	}
	if (rank==0)
		printf("Attempting to gather domains. \n");
	gather_domain(masterimage, rank, nx, ny);
	if (rank==0)
		printf("Successfully gathered domains. \n");
	if (rank==0){
		write_success=pgmwrite(outfilename, masterimage, nx, ny);
	}
	MPI_Bcast(&write_success, 1, MPI_INT, 0, MPI_COMM_WORLD);
	return write_success;
}
