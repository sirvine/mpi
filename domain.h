/* 
 * File:   single_domain.h
 * Author: h
 *
 * Created on 02 November 2013, 21:17
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "arralloc.h"

#ifndef DOMAIN_H
#define	DOMAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 Dimensions of internal image, these are equal to dimensions of input image + 2.
 */	
int x_dim, y_dim;

/*
 We must keep two copies of data to perform some algorithms. 
 */
float **edges, **old_image, **new_image;

/*
 This routine determines the layout of domain decompositions.
 */
int init_domain(const int size, const int rank, const int nx, const int ny);

/*
 We must distribute edges prior to domain construction. Domain must already be initialised. 
 * Memory is allocated and data communicated or initialised appropriately. 
 */
int populate_domain(const float *masteredges, const int rank, const int nx, const int ny);

/*
 Once we have computed our image, we wish to gather data from all processes. 
 */
int gather_domain(float *masterimage, const int rank, const int nx, const int ny);

/*
 Frees memory associated with this domain.
 */
int free_domain();

/*
 We begin the process of swapping halos. The main loop is processed while communications are under way. 
 */
int init_halo_swap();

/*
 We ensure data is received before continuing on. 
 */
int complete_halo_swap();

/*
 Computes the maximal delta between old and new images. 
 */
float termination_delta();

#ifdef	__cplusplus
}
#endif

#endif	/* DOMAIN_H */


