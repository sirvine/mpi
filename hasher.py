import hashlib
BLOCKSIZE = 65536
hasher = hashlib.sha1()
with open('image768x1152aa.pgm', 'rb') as afile:
    buf = afile.read(BLOCKSIZE)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(BLOCKSIZE)
print(hasher.hexdigest())


