/* 
 * File:   util.h
 * Author: h
 *
 * Created on 25 November 2013, 15:24
 */
#include <mpi.h>
#include <malloc.h>

#ifndef UTIL_H
#define	UTIL_H

#ifdef	__cplusplus
extern "C" {
#endif
	
/*
 * A wall-clock timer type. 
 */	
typedef struct timer{
	int n_samples;
	int capacity;
	double *samples;
}Timer;

void allocate_timer(Timer *t, int capacity);

/*
 Start timing prior to measurements. 
 */
void start_timer(Timer *t);

/*
 Take a measurement at current wall-clock time.
 */
void take_measurement(Timer *t);

/*
 Returns mean of measured periods. 
 */
double mean_period(Timer *t);

/*
 Returns variance of measured periods.
 */
double var_period(Timer *t);

void free_timer(Timer *t);


/*
 Swap the pointed location between a and b. 
 */
void swap_ptr(void **a, void **b);

#ifdef	__cplusplus
}
#endif

#endif	/* UTIL_H */


