from math import sqrt;
import os
from matplotlib import pyplot as plt


def readdir(dirname, outlist, nround):
	for f in os.listdir(dirname):
		if ".pgm.sge.o" in f:
			next=open(dirname+"/"+f, "r")
			lines=next.readlines()
			core=(int(lines[4].split()[4]))
			dim=([int(lines[5].split()[3].split(",")[0]), int(lines[5].split()[4].split(".")[0])])
			domain=([int(lines[6].split()[4].split("x")[0]), int(lines[6].split()[4].split("x")[1].split(".")[0])])
			time=(float(lines[10].split()[2].split(",")[0]))
			var=(float(lines[10].split()[4]))
			err=(sqrt(var))/sqrt(5);
			freq=(nround/time)*dim[0]*dim[1]/1e9
			dfreq=(nround*err/(time*time))*dim[0]*dim[1]/1e9
			bandwidth=2*((domain[0]-1)*dim[1]+(domain[1]-1)*dim[0])*nround/(time*1e9)/core;
			dbandwidth=2*((domain[0]-1)*dim[1]+(domain[1]-1)*dim[0])*nround*err/(time*time*1e9)/core;
			freqcore=freq/core
			dfreqcore=dfreq/core
			outlist.append([core, nround, dim, domain, time, var, err, freq, dfreq, bandwidth, dbandwidth, freqcore, dfreqcore]);

large=[]
medium=[]
small=[]

readdir("job20000l", large, 40000);
readdir("job10000l", large, 10000);
readdir("job5000l", large, 5000);

readdir("job80000m", medium, 80000);
readdir("job40000m", medium, 40000);
readdir("job20000m", medium, 20000);

readdir("job640000s", small, 320000);
readdir("job320000s", small, 320000);
readdir("job160000s", small, 160000);

large=sorted(large)
medium=sorted(medium)
small=sorted(small)

largefreqs=[]
dlargefreqs=[]
largecores=[]
largeband=[]
dlargeband=[]
largefc=[]
dlargefc=[]

mediumfreqs=[]
dmediumfreqs=[]
mediumcores=[]
mediumband=[]
dmediumband=[]
mediumfc=[]
dmediumfc=[]

smallfreqs=[]
dsmallfreqs=[]
smallcores=[]
smallband=[]
dsmallband=[]
smallfc=[]
dsmallfc=[]

for l in large:
	largefreqs.append(l[7])
	largecores.append(l[0])
	dlargefreqs.append(l[8])
	largeband.append(l[9])
	dlargeband.append(l[10])
	largefc.append(l[11])
	dlargefc.append(l[12])

for m in medium:
	mediumfreqs.append(m[7])
	mediumcores.append(m[0])
	dmediumfreqs.append(m[8])
	mediumband.append(m[9])
	dmediumband.append(m[10])
	mediumfc.append(m[11])
	dmediumfc.append(m[12])

for s in small:
	smallfreqs.append(s[7])
	smallcores.append(s[0])
	dsmallfreqs.append(s[8])
	smallband.append(s[9])
	dsmallband.append(s[10])
	smallfc.append(s[11])
	dsmallfc.append(s[12])

plt.figure(0);
plt.errorbar(largecores, largefreqs, yerr=dlargefreqs, label="768x1152")
plt.errorbar(mediumcores, mediumfreqs, yerr=dmediumfreqs, label="512x384")
plt.errorbar(smallcores, smallfreqs, yerr=dsmallfreqs, label="192x128")
plt.xlabel('Processes', size=20)
plt.ylabel('Performance (Billion Pixel/s)', size=20)
plt.legend(loc=2);


plt.figure(1);
plt.errorbar(largeband, largefc, xerr=dlargeband, yerr=dlargefc, label="768x1152", ls='none')
plt.errorbar(mediumband, mediumfc, xerr=dmediumband, yerr=dmediumfc, label="512x384", ls='none')
plt.errorbar(smallband, smallfc, xerr=dsmallband, yerr=dsmallfc, label="192x128", ls='none')
plt.xlabel('Bandwidth per Process (Billion Pixels/s)', size=20)
plt.ylabel('Performance per Process (Billion Pixel/s)', size=20)
plt.legend(loc=1);
plt.show();
	
