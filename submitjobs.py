import os;
import time;

names=[#["edge192x128.pgm", "image192x128.pgm"]]; 
#["edge512x384.pgm", "image512x384.pgm"]]; 
["edge768x1152.pgm", "image768x1152.pgm"]];

for name in names:
	for np in range(16, 33):
		jobname="s"+str(np)+name[1]+".sge"
		job=open(jobname, 'w')
		job.write('#!/bin/bash\n')
		job.write('#$ -cwd -V\n')
		job.write('MPIEXE="./ImageReconstruction"\n')
		job.write('echo "--------------------------------------------------"\n')
		job.write('echo "Running MPI program <$MPIEXE> on" $NSLOTS "processes"\n')
		job.write('echo "--------------------------------------------------"\n')
		job.write('echo\n')
		job.write('\n')
		job.write('(time mpiexec -n $NSLOTS ./$MPIEXE -f '+name[0]+' -o '+name[1]+' ) 2>&1\n')
		job.write('echo\n')
		job.write('echo "--------------------"\n')
		job.write('echo "Finished MPI program"\n')
		job.write('echo "--------------------"\n')
		job.write('echo\n')
		job.close();
		print 'qsub -pe mpi '+str(np)+' '+jobname
		os.system('qsub -pe mpi '+str(np)+' '+jobname)
		time.sleep(30);
