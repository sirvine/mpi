
#ifndef SINGLEDOMAIN

#include <malloc.h>

#include "domain.h"
#include <mpi.h>
#include "arralloc.h"
#include "util.h"

int cart_dims[2];
int cart_periods[2]={0, 0};
int cart_coords[2];

MPI_Comm grid_comm;

MPI_Request halo_send_requests[4]={MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL};
MPI_Request halo_receive_requests[4]={MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL};

MPI_Datatype x_halo;
MPI_Datatype y_halo;


#ifdef USE_BSEND
float *bsend;
#endif

/*
 This type defines the boundaries of a 2d domain. 
*/
typedef struct domain_range{
	int xstart, xend, ystart, yend;
}Domain_Range;

/*
 Given a domain coordinate (i, j) and image bounds (nx, ny) we compute the decomposition bounds.
 */
Domain_Range *rangeof(int i, int j, int nx, int ny, Domain_Range *range){
	range->xstart=i*(nx/cart_dims[0])+(i<nx%cart_dims[0]?i:nx%cart_dims[0]);
	range->xend=(i+1)*(nx/cart_dims[0])+((i+1)<nx%cart_dims[0]?(i+1):nx%cart_dims[0]);
	range->ystart=j*(ny/cart_dims[1])+(j<ny%cart_dims[1]?j:ny%cart_dims[1]);
	range->yend=(j+1)*(ny/cart_dims[1])+((j+1)<ny%cart_dims[1]?(j+1):ny%cart_dims[1]);
	return range;
}

/*
 Compute area of a domain. 
 */
int rangecount(Domain_Range *range){
	return (range->xend-range->xstart)*(range->yend-range->ystart);
}

/*
 Given global indices (k, l) we can compute local index of domain. 
 */
int indexof(int k, int l, Domain_Range *range){
	return (k-range->xstart)*(range->yend-range->ystart)+l-range->ystart;
}

int init_domain(const int size, const int rank, const int nx, const int ny){
	int factors[32];
	int	min_circumference=0x7FFFFFFF;
	int nfactors=0;
	
	for (int i=1;i<=size;i++){//compute all integer factors of size
		if (size%i==0){
			factors[nfactors++]=i;
		}
	}
	
	for (int i=0;i<nfactors;i++){//find factors minimising the total halo circumference
		int xfactor=factors[i];
		int yfactor=(size/factors[i]);
		int halo_circumference=(xfactor-1)*ny+(yfactor-1)*nx;
		if (halo_circumference<min_circumference){
			min_circumference=halo_circumference;
			cart_dims[0]=xfactor;//We end up with the best choice of decomposition.
			cart_dims[1]=yfactor;
		}
	}
	
	MPI_Cart_create(MPI_COMM_WORLD, 2, cart_dims, cart_periods, 1, &grid_comm);
	MPI_Cart_get(grid_comm, 2, cart_dims, cart_periods, cart_coords);
	
	Domain_Range range;
	rangeof(cart_coords[0], cart_coords[1], nx, ny, &range);
	x_dim=range.xend-range.xstart+2;
	y_dim=range.yend-range.ystart+2;

	MPI_Type_vector(x_dim-2, 1, y_dim, MPI_FLOAT, &x_halo);
	MPI_Type_vector(1, y_dim-2, 0, MPI_FLOAT, &y_halo);
	MPI_Type_commit(&x_halo);
	MPI_Type_commit(&y_halo);
	
	if (rank==0){
		printf("Constructed domain grid of %dx%d. \n", cart_dims[0], cart_dims[1]);
	}
	return 0;
}

int populate_domain(const float *masteredges, const int rank, const int nx, const int ny){
	
	MPI_Request **requests;
	float ***domains;
	
	edges=arralloc(sizeof(float), 2, x_dim, y_dim);
	new_image=arralloc(sizeof(float), 2, x_dim, y_dim);
	old_image=arralloc(sizeof(float), 2, x_dim, y_dim);
	

	
#ifdef USE_BSEND
	bsend=malloc(sizeof(float)*(x_dim*2+y_dim*2-8)+4*MPI_BSEND_OVERHEAD);
	MPI_Buffer_attach(bsend, sizeof(float)*(x_dim*2+y_dim*2-8)+4*MPI_BSEND_OVERHEAD);
#endif
	
	for (int i=0;i<x_dim;i++){
		for (int j=0;j<y_dim;j++){
			old_image[i][j]=new_image[i][j]=255.0;
		}
	}

	if (rank==0){//dish out the buffer
		domains=arralloc(sizeof(float*), 2, cart_dims[0], cart_dims[1]);
		requests=arralloc(sizeof(MPI_Request), 2, cart_dims[0], cart_dims[1]);
		for (int i=0;i<cart_dims[0];i++){
			for (int j=0;j<cart_dims[1];j++){
				Domain_Range range;
				rangeof(i, j, nx, ny, &range);
				domains[i][j]=calloc(rangecount(&range), sizeof(float));
				for (int k=range.xstart;k<range.xend;k++){
					for (int l=range.ystart;l<range.yend;l++){
						domains[i][j][indexof(k, l, &range)]=masteredges[k*ny+l];
					}
				}
				int dest_rank;
				int dest_coords[2]={i, j};
				MPI_Cart_rank(grid_comm, dest_coords, &dest_rank);
				MPI_Isend(domains[i][j], rangecount(&range), MPI_FLOAT, dest_rank, 0, grid_comm, &requests[i][j]);
			}
		}
	}

	float *edgereceive=calloc((x_dim-2)*(y_dim-2), sizeof(float));
	MPI_Recv(edgereceive, (x_dim-2)*(y_dim-2), MPI_FLOAT, 0, 0, grid_comm, MPI_STATUS_IGNORE);
	
	for (int i=1;i<x_dim-1;i++){
		for (int j=1;j<y_dim-1;j++){
			edges[i][j]=edgereceive[(i-1)*(y_dim-2)+j-1];
		}
	}
	
	free(edgereceive);
	if (rank==0){
		for (int i=0;i<cart_dims[0];i++){
			for (int j=0;j<cart_dims[1];j++){
				MPI_Wait(&requests[i][j], MPI_STATUS_IGNORE);
				free(domains[i][j]);
			}
		}
		free(domains);
		free(requests);
	}
	return 0;
}

int gather_domain(float *masterimage, const int rank, const int nx, const int ny){
	MPI_Request **requests;
	float ***domains;

	if (rank==0){
		domains=arralloc(sizeof(float*), 2, cart_dims[0], cart_dims[1]);
		requests=arralloc(sizeof(MPI_Request), 2, cart_dims[0], cart_dims[1]);
		for (int i=0;i<cart_dims[0];i++){
			for (int j=0;j<cart_dims[1];j++){
				Domain_Range range;
				rangeof(i, j, nx, ny, &range);
				domains[i][j]=calloc(rangecount(&range), sizeof(float));
				int source_rank;
				int source_coords[2]={i, j};
				MPI_Cart_rank(grid_comm, source_coords, &source_rank);
				MPI_Irecv(domains[i][j], rangecount(&range), MPI_FLOAT, source_rank, 0, grid_comm, &requests[i][j]);
			}
		}
	}
	float *sendimage=calloc((x_dim-2)*(y_dim-2), sizeof(float));
	for (int i=0;i<x_dim-2;i++){
		for (int j=0;j<y_dim-2;j++){
			sendimage[i*(y_dim-2)+j]=old_image[i+1][j+1];
		}
	}
	MPI_Ssend(sendimage, (x_dim-2)*(y_dim-2), MPI_FLOAT, 0, 0, grid_comm);
	free(sendimage);
	if (rank==0){
		for (int i=0;i<cart_dims[0];i++){
			for (int j=0;j<cart_dims[1];j++){
				MPI_Wait(&requests[i][j], MPI_STATUS_IGNORE);
				Domain_Range range;
				rangeof(i, j, nx, ny, &range);
				for (int k=range.xstart;k<range.xend;k++){
					for (int l=range.ystart;l<range.yend;l++){
						masterimage[k*ny+l]=domains[i][j][indexof(k, l, &range)];
					}
				}
				free(domains[i][j]);
			}
		}
		free(domains);
		free(requests);
	}
	
	return 0;
	
	
	if (rank==0){
		for (int i=0;i<x_dim-2;i++){
			for (int j=0;j<y_dim-2;j++){
				masterimage[i*ny+j]=old_image[i+1][j+1];
			}
		}
	}
	
	return 0;
}

int free_domain(){
	free(edges);
	free(new_image);
	free(old_image);
	MPI_Type_free(&x_halo);
	MPI_Type_free(&y_halo);
#ifdef USE_BSEND
	int size;
	float *dummy;
	MPI_Buffer_detach(dummy, &size);
	free(bsend);
#endif
	return 0;
}

int init_halo_swap(){
	MPI_Waitall(4, halo_send_requests, MPI_STATUSES_IGNORE);
	int source_rank, dest_rank;
	MPI_Datatype types[4]={y_halo, y_halo, x_halo, x_halo};
	float *send_pointers[4]={&new_image[1][1], &new_image[x_dim-2][1], &new_image[1][1], &new_image[1][y_dim-2]};
	for (int i=0;i<4;i++){
		MPI_Cart_shift(grid_comm, i/2, (i%2)*2-1, &source_rank, &dest_rank);
#ifndef USE_BSEND
		MPI_Isend(send_pointers[i], 1, types[i], dest_rank, 0, grid_comm, &halo_send_requests[i]);
#else
		MPI_Ibsend(send_pointers[i], 1, types[i], dest_rank, 0, grid_comm, &halo_send_requests[i]);
#endif
	}
	float *recv_pointers[4]={&old_image[x_dim-1][1], &old_image[0][1], &old_image[1][y_dim-1], &old_image[1][0]};
	for (int i=0;i<4;i++){
		MPI_Cart_shift(grid_comm, i/2, (i%2)*2-1, &source_rank, &dest_rank);
		MPI_Irecv(recv_pointers[i], 1, types[i], source_rank, 0, grid_comm, &halo_receive_requests[i]);
	}
	return 0;
}


int complete_halo_swap(){
	MPI_Waitall(4, halo_receive_requests, MPI_STATUSES_IGNORE);
	return 0;
}

float termination_delta(){
	float delta=0.0, glob_delta;
	for (int i=1;i<x_dim-1;i++){
		for (int j=1;j<y_dim-1;j++){
			float next=fabsf(new_image[i][j]-old_image[i][j]);
			if (delta<next){
				delta=next;
			}
		}
	}
	MPI_Allreduce(&delta, &glob_delta, 1, MPI_FLOAT, MPI_MAX, grid_comm);
	return glob_delta;
}


#endif /* SINGLEDOMAIN */
