/* 
 * File:   main.c
 * Author: h
 *
 * Created on 02 November 2013, 19:23
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>

#include "image_reconstruction.h"

/*
 * Argument parsing, MPI initialisation as well as clean code termination. 
 * Program arguments are -f "inputfilename", -o "outputfilename"
 */
int main(int argc, char** argv) {
	int size, rank;
	char *infilename=NULL;
	char *outfilename=NULL;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	if (rank==0)
		printf("Running Image Reconstruction on %d processes. \n", size);
	for (int i=0;i<argc;i++){
		if (argv[i][0]=='-'&&argc>i+1){
			switch(argv[i][1]){
				case 'f':
					infilename=argv[i+1];
					break;
				case 'o':
					outfilename=argv[i+1];
					break;
				default:
					break;
			}
			i++;
		}
	}
	if (outfilename==NULL){
		if (rank==0)
			printf("Please specify an output filename! \n");
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	if (infilename==NULL){
		if (rank==0)
			printf("Please specify an input filename! \n");
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	
	
	/*Core of program begins here*/
	if (read_edges(size, rank, infilename)!=0){
		if (rank==0)
			printf("Failed to read edges. \n");
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	if (compute_image(size, rank)!=0){
		if (rank==0)
			printf("Failed to compute image. \n");
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	if (write_image(size, rank, outfilename)!=0){
		if (rank==0)
			printf("Failed to write image. \n");
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	
	
	/*Cleanup here*/
	if (rank==0)
		printf("Job done! :) \n");
	MPI_Finalize();
	return (EXIT_SUCCESS);
}

