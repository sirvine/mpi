/* 
 * File:   image_reconstruction.h
 * Author: h
 *
 * Created on 10 November 2013, 23:17
 */

#include <stdlib.h>
#include <malloc.h>
#include <time.h>

#include "pgmio.h"
#include "domain.h"

#ifndef IMAGE_RECONSTRUCTION_H
#define	IMAGE_RECONSTRUCTION_H

#ifdef	__cplusplus
extern "C" {
#endif
	
int nx, ny;

/*
 Read in edges from input file.
 */
int read_edges(const int size, const int rank, const char *infilename);

/*
 Compute image from edges. 
 */
int compute_image(const int size, const int rank);

/*
 * Write image to output file.
 */	
int write_image(const int size, const int rank, const char *outfilename);


#ifdef	__cplusplus
}
#endif

#endif	/* IMAGE_RECONSTRUCTION_H */


