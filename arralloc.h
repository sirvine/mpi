/* 
 * File:   arralloc.h
 * Author: h
 *
 * Created on 02 November 2013, 19:33
 */

#ifndef ARRALLOC_H
#define	ARRALLOC_H

#ifdef	__cplusplus
extern "C" {
#endif
	
void *arralloc(size_t size, int ndim, ...);

#ifdef	__cplusplus
}
#endif

#endif	/* ARRALLOC_H */

