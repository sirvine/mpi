
OPT   +=  -DTERMINATION_DELTA=0.01 #When does code terminate

OPT   +=  -DBENCHMARK   #Comment this to run until convergence. 

OPT   +=  -DBENCHMARK_ROUNDS=40000  #Number of rounds to perform per benchmark

#OPT   +=  -DSINGLEDOMAIN   #Uncomment this to test code for single domain

#OPT   +=  -DUSE_BSEND  #May be required on certain platforms

#MKDIR   =   mkdir
#CP      =   cp 
#CCADMIN =   CCadmin
MF      =   Makefile
 
CC      =   mpicc

CFLAGS  =   -g -fastsse -w

LFLAGS  =   -lm

EXE     =    ImageReconstruction

SRC= \
	main.c pgmio.c \
	single_domain.c tiled_domain.c \
	image_reconstruction.c \
	util.c arralloc.c	

.SUFFIXES:
.SUFFIXES: .c .o

OBJ     =   $(SRC:.c=.o)

.c.o:
	$(CC) $(CFLAGS) $(OPT) -c $<

all:	$(EXE)

$(OBJ):	$(INC)

$(EXE):	$(OBJ)
	$(CC) $(CFLAGS) $(OPT) -o $@ $(OBJ) $(LFLAGS)

$(OBJ):	$(MF)

clean:
	rm -f $(OBJ) $(EXE) core


# build
#build: .build-post

#.build-pre: 
	

#.build-post: .build-impl
# Add your post 'build' code here...


# clean
#clean: .clean-post

#.clean-pre:
# Add your pre 'clean' code here...

#.clean-post: .clean-impl
# Add your post 'clean' code here...


# clobber
#clobber: .clobber-post

#.clobber-pre:
# Add your pre 'clobber' code here...

#.clobber-post: .clobber-impl
# Add your post 'clobber' code here...


# all
#all: .all-post

#.all-pre:
# Add your pre 'all' code here...

#.all-post: .all-impl
# Add your post 'all' code here...


# build tests
#build-tests: .build-tests-post

#.build-tests-pre:
# Add your pre 'build-tests' code here...

#.build-tests-post: .build-tests-impl
# Add your post 'build-tests' code here...


# run tests
#test: .test-post

#.test-pre: build-tests
# Add your pre 'test' code here...

#.test-post: .test-impl
# Add your post 'test' code here...


# help
#help: .help-post

#.help-pre:
# Add your pre 'help' code here...

#.help-post: .help-impl
# Add your post 'help' code here...



# include project implementation makefile
#include nbproject/Makefile-impl.mk

# include project make variables
#include nbproject/Makefile-variables.mk
